﻿using HarmonyLib;
using RimWorld;
using System;
using Verse;

namespace PowerMill
{
    [DefOf]
	public static class PowerMillDefOf
    {
        public static ThingDef PM_PowerMill;
        public static JobDef PM_RopeAnimalToPowerMill;
        public static JobDef PM_GeneratePower;
    }
	public class PowerMillMod : Mod
    {
        public PowerMillMod(ModContentPack pack) : base(pack)
        {
            new Harmony("PowerMill.Mod").PatchAll();
        }
    }

	[AttributeUsage(AttributeTargets.Class | AttributeTargets.Struct)]
	public class HotSwappableAttribute : Attribute
	{
	}
}
