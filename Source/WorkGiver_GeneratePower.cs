﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;

namespace PowerMill
{

    public class WorkGiver_GeneratePower : WorkGiver_Scanner
	{
        public override ThingRequest PotentialWorkThingRequest => ThingRequest.ForDef(PowerMillDefOf.PM_PowerMill);
        public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            var comp = t.TryGetComp<CompPowerMill>();
            if (comp != null && comp.trunks.Any(x => x.worker is null) && comp.totalWorkers?.Count < 4)
            {
                return true;
            }
            return false;
        }

        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            return JobMaker.MakeJob(PowerMillDefOf.PM_GeneratePower, t);
		}
	}
}
