﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using Verse;

namespace PowerMill
{
    [HotSwappableAttribute]
	[StaticConstructorOnStartup]
	public class CompPowerMill : CompPowerPlant
	{
		public List<Trunk> trunks;

		public List<Pawn> totalWorkers;

		public static readonly Material TrunkMat = MaterialPool.MatFrom("Things/Building/Power/PowerMill/Trunk_c");
		public Vector3 BasePolePosOffset => Altitudes.AltIncVect + new Vector3(0, 0, 0.5f);

		protected override float DesiredPowerOutput => 600 * GetSpinPower();

		public override void PostSpawnSetup(bool respawningAfterLoad)
		{
			base.PostSpawnSetup(respawningAfterLoad);
			if (trunks is null)
            {
				trunks = new List<Trunk>();
				for (var i = 0; i < 4; i++)
                {
					trunks.Add(new Trunk
                    {
						curRotation = 90f * i
                    });
                }
            }
			if (this.totalWorkers is null)
            {
				this.totalWorkers = new List<Pawn>();
			}
		}

		public override void CompTick()
        {
            base.CompTick();
			this.totalWorkers.RemoveAll(delegate (Pawn p)
			{
				if (p.RaceProps.Animal)
                {
					var ropedPawn = p.roping.RopedByPawn;
					if (ropedPawn != null && ropedPawn.CurJobDef != PowerMillDefOf.PM_RopeAnimalToPowerMill)
					{
						Log.Message("1 removing worker: " + p);
						return true;
                    }
					else if (ropedPawn is null && p.CurJobDef != PowerMillDefOf.PM_GeneratePower)
					{
						Log.Message("2 removing worker: " + p);
						return true;
                    }
                }
				else if (p.CurJobDef != PowerMillDefOf.PM_GeneratePower)
                {
					Log.Message("3 removing worker: " + p);
					return true;
				}
				return false;
			});

			float spinSpeed = GetSpinPower();
            foreach (var trunk in trunks)
            {
                trunk.curRotation += spinSpeed;
                if (trunk.curRotation > 360)
                {
                    var diff = trunk.curRotation - 360;
                    trunk.curRotation = diff;
                }
            }
        }

        private float GetSpinPower()
        {
            var busyTrunks = trunks.Where(x => x.worker != null).ToList();
            var spinSpeed = busyTrunks.Count > 0 ? Mathf.Max(Mathf.Log10(10 * busyTrunks
                .Sum(x => x.worker.GetStatValue(StatDefOf.MoveSpeed) * x.worker.BodySize) / (4.5f * 4)), 0.01f) : 0;
            return spinSpeed;
        }

        public override void PostDraw()
		{
			base.PostDraw();
			for (var i = 0; i < trunks.Count; i++)
            {
				var trunk = trunks[i];
				Vector3 s = new Vector3(3.77f, 1f, 3.77f);
				Matrix4x4 matrix = default(Matrix4x4);
				var pos = this.parent.DrawPos + BasePolePosOffset + (Quaternion.AngleAxis(trunk.curRotation, Vector3.up) * (Vector3.forward * 1.8f));
				pos.y = this.parent.DrawPos.y;
				if (trunk.curRotation >= 290 && trunk.curRotation <= 360 || trunk.curRotation <= 60)
				{
					pos.y -= 1;
				}
				else
				{
					pos.y += 1;
				}
				matrix.SetTRS(pos, Quaternion.Euler(0, trunk.curRotation, 0), s);
				Graphics.DrawMesh(MeshPool.plane10, matrix, TrunkMat, 0);
			}
		}

        public override void PostExposeData()
        {
            base.PostExposeData();
			Scribe_Collections.Look(ref trunks, "trunks", LookMode.Deep);
			Scribe_Collections.Look(ref totalWorkers, "futureWorkers", LookMode.Reference);
		}
	}
}
