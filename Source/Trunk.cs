﻿using Verse;

namespace PowerMill
{
    public class Trunk : IExposable
    {
		public float curRotation;

		public Pawn worker;
        public void ExposeData()
        {
			Scribe_Values.Look(ref curRotation, "curRotation");
			Scribe_References.Look(ref worker, "worker");
        }
    }
}
