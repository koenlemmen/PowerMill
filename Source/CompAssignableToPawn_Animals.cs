﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using Verse;

namespace PowerMill
{
    public class CompAssignableToPawn_Animals : CompAssignableToPawn
	{
		public override IEnumerable<Pawn> AssigningCandidates
		{
			get
			{
				if (!parent.Spawned)
				{
					return Enumerable.Empty<Pawn>();
				}
				return parent.Map.mapPawns.SpawnedColonyAnimals;
			}
		}

        protected override string GetAssignmentGizmoLabel()
        {
            return "PM.AssignAnimals".Translate();
		}
    }
}
