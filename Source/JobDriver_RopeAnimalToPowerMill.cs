﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;

namespace PowerMill
{
    public class JobDriver_RopeAnimalToPowerMill : JobDriver_RopeToDestination
	{
		protected CompPowerMill compPowerMill => PowerMill.TryGetComp<CompPowerMill>();
		protected Building PowerMill => base.TargetThingB as Building;
		protected IntVec3 PowerMillPosition => PowerMill.Position;
		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			if (!pawn.roping.Ropees.NullOrEmpty())
			{
				foreach (Pawn ropee in pawn.roping.Ropees)
				{
					pawn.Reserve(ropee, job, 1, -1, null, errorOnFailed);
				}
			}
			return true;
		}

		protected override bool HasRopeeArrived(Pawn ropee, bool roperWaitingAtDest)
		{
			if (!PowerMillPosition.IsValid)
			{
				return false;
			}
			if (!pawn.Position.InHorDistOf(PowerMillPosition, 2f))
			{
				return false;
			}
			District district = PowerMillPosition.GetDistrict(pawn.Map);
			if (district != pawn.GetDistrict() || district != ropee.GetDistrict())
			{
				return false;
			}
			return true;
		}


        public override void Notify_Starting()
        {
            base.Notify_Starting();
			foreach (Pawn ropee in pawn.roping.Ropees)
			{
				if (!compPowerMill.totalWorkers.Contains(ropee))
				{
					compPowerMill.totalWorkers.Add(ropee);
				}
			}
		}
        protected override IEnumerable<Toil> MakeNewToils()
        {
			this.FailOn(delegate
			{
				return compPowerMill.trunks.All(x => x.worker != pawn && x.worker != null);
			});
			AddFinishAction(delegate
			{
				foreach (Pawn ropee in pawn.roping.Ropees)
				{
					compPowerMill.totalWorkers.Remove(ropee);
				}
			});
			return base.MakeNewToils();
        }
        protected override void ProcessArrivedRopee(Pawn ropee)
		{
			if (PowerMillPosition.IsValid)
			{
				var job = JobMaker.MakeJob(PowerMillDefOf.PM_GeneratePower, PowerMill);
				ropee.jobs.TryTakeOrderedJob(job);
			}
		}

		protected override bool ShouldOpportunisticallyRopeAnimal(Pawn animal)
		{
			return false;
		}
	}
}
