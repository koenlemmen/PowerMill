﻿using RimWorld;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEngine;
using Verse;
using Verse.AI;

namespace PowerMill
{
    [HotSwappable]
    public class JobDriver_GeneratePower : JobDriver
	{
		protected CompPowerMill compPowerMill => job.targetA.Thing.TryGetComp<CompPowerMill>();
		public override bool TryMakePreToilReservations(bool errorOnFailed)
		{
			return true;
		}

        public override void Notify_Starting()
        {
            base.Notify_Starting();
			if (!compPowerMill.totalWorkers.Contains(pawn))
            {
				compPowerMill.totalWorkers.Add(pawn);
			}
		}
		protected override IEnumerable<Toil> MakeNewToils()
		{
			this.FailOn(delegate
			{
				if (pawn.RaceProps.Animal && !job.targetA.Thing.TryGetComp<CompAssignableToPawn_Animals>().AssignedPawns.Contains(pawn))
                {
					return true;
                }
				return compPowerMill.trunks.All(x => x.worker != pawn && x.worker != null);
			});
			this.FailOnDespawnedOrNull(TargetIndex.A);
			Toil gotoPowerMill = new Toil();
			gotoPowerMill.initAction = delegate
			{
				if (base.Map.reservationManager.CanReserve(pawn, TargetA))
				{
					pawn.Reserve(TargetA, job);
				}
				pawn.pather.StartPath(TargetA, PathEndMode.Touch);
			};
			gotoPowerMill.FailOnDespawnedOrNull(TargetIndex.A);
			gotoPowerMill.defaultCompleteMode = ToilCompleteMode.PatherArrival;
			gotoPowerMill.atomicWithPrevious = true;
			yield return gotoPowerMill;
			var generatePower = new Toil();
			generatePower.tickAction = delegate
			{
				var comp = compPowerMill;
				var trunk = comp.trunks.FirstOrDefault(x => x.worker == pawn);
				if (trunk != null)
				{
					if (trunk.curRotation > 290 || trunk.curRotation <= 20)
                    {
						pawn.Rotation = Rot4.East;
                    }
					else if (trunk.curRotation > 20 && trunk.curRotation <= 110)
					{
						pawn.Rotation = Rot4.South;
					}
					else if (trunk.curRotation > 110 && trunk.curRotation <= 200)
					{
						pawn.Rotation = Rot4.West;
					}
					else if (trunk.curRotation > 200 && trunk.curRotation <= 290)
					{
						pawn.Rotation = Rot4.North;
					}
					pawn.Position = pawn.DrawPos.ToIntVec3();
				}
				else
                {
					if (comp.trunks.All(x => x.worker is null))
					{
						foreach (var trunk2 in comp.trunks)
                        {
							AddDistanceRecord(comp, trunk2);
						}
						var closestTrunk = distanceRecords.OrderBy(x => x.Value.curValue).FirstOrDefault().Key;
						closestTrunk.worker = pawn;
						distanceRecords.Clear();
					}
					else
                    {
						foreach (var availableTrunk in comp.trunks.Where(x => x.worker is null))
                        {
                            AddDistanceRecord(comp, availableTrunk);
                        }

						var closestTrunk = distanceRecords.OrderBy(x => x.Value.curValue).First();
						if (closestTrunk.Value.curValue > closestTrunk.Value.minValue && closestTrunk.Value.curValue < 1 
							&& closestTrunk.Value.minValue < 99999)
                        {
							closestTrunk.Key.worker = pawn;
							distanceRecords.Clear();
						}
					}
				}
				if (pawn.RaceProps.Humanlike)
                {
					if (this.debugTicksSpentThisToil > 1000 && pawn.IsHashIntervalTick(GenDate.TicksPerHour))
					{
						pawn.jobs.CheckForJobOverride();
					}
				}
				else
                {
					if (this.debugTicksSpentThisToil > 1000 && pawn.IsHashIntervalTick(GenDate.TicksPerHour * 5))
					{
						pawn.jobs.CheckForJobOverride();
					}
				}
			};
			generatePower.handlingFacing = true;
			generatePower.defaultCompleteMode = ToilCompleteMode.Never;
			yield return generatePower;
			AddFinishAction(delegate
			{
				var trunk = compPowerMill.trunks.FirstOrDefault(x => x.worker == pawn);
				if (trunk != null)
				{
					trunk.worker = null;
				}
				compPowerMill.totalWorkers.Remove(pawn);
			});
		}

        private void AddDistanceRecord(CompPowerMill comp, Trunk trunk)
        {
            var forcedPos = comp.parent.DrawPos + comp.BasePolePosOffset +
                (Quaternion.AngleAxis(trunk.curRotation, Vector3.up) * (Vector3.forward * 1f));
			if (pawn.RaceProps.Humanlike)
            {
				forcedPos += Quaternion.AngleAxis(trunk.curRotation, Vector3.up) * (Vector3.left * 0.3f);
			}
			var distance = Vector3.Distance(forcedPos.Yto0(), pawn.DrawPos.Yto0());
            if (distanceRecords is null)
			{
				distanceRecords = new Dictionary<Trunk, DistanceRecord>();
            }

            if (!distanceRecords.TryGetValue(trunk, out var distanceRecord))
            {
                distanceRecords[trunk] = new DistanceRecord
                {
					minValue = 99999,
                    curValue = distance
                };
            }
            else
			{
				if (distanceRecord.curValue < distanceRecord.minValue)
                {
					distanceRecord.minValue = distanceRecord.curValue;
				}
				distanceRecord.curValue = distance;
			}
		}

        public class DistanceRecord
        {
			public float minValue = -1;
			public float curValue;
        }
		public Dictionary<Trunk, DistanceRecord> distanceRecords = new Dictionary<Trunk, DistanceRecord>();
        public override Vector3 ForcedBodyOffset
        {
            get
            {
				var comp = compPowerMill;
				var trunk = comp.trunks.FirstOrDefault(x => x.worker == pawn);
				if (trunk != null)
				{
					var diff = comp.parent.DrawPos - DrawPos(pawn);
					var forcedPos = diff + comp.BasePolePosOffset + (Quaternion.AngleAxis(trunk.curRotation, Vector3.up) * (Vector3.forward * 1.8f));
					if (pawn.RaceProps.Humanlike)
                    {
						forcedPos += Quaternion.AngleAxis(trunk.curRotation, Vector3.up) * (Vector3.left * 0.3f);
					}
					forcedPos.y = 0;
					if (trunk.curRotation > 290 || trunk.curRotation <= 20)
					{
						forcedPos.y = 300;
					}
					else if (trunk.curRotation > 20 && trunk.curRotation <= 110)
					{
				
					}
					else if (trunk.curRotation > 110 && trunk.curRotation <= 200)
					{
				
					}
					else if (trunk.curRotation > 200 && trunk.curRotation <= 290)
					{
				
					}
					return forcedPos;
				}
				return Vector3.zero;
			}
        }

		public Vector3 DrawPos(Pawn pawn)
		{
			pawn.Drawer.tweener.PreDrawPosCalculation();
			Vector3 tweenedPos = pawn.Drawer.tweener.TweenedPos;
            //tweenedPos += pawn.Drawer.jitterer.CurrentOffset;
            JitterHandler jitterer = GetInstanceField(typeof(Pawn_DrawTracker), pawn.Drawer, "jitterer") as JitterHandler;
            Vector3 jitterOffset = jitterer.CurrentOffset;
            tweenedPos += jitterOffset;
            tweenedPos += pawn.Drawer.leaner.LeanOffset;
			tweenedPos.y = pawn.def.Altitude;
			return tweenedPos;
		}

        internal static object GetInstanceField(Type type, object instance, string fieldName)
        {
            BindingFlags bindFlags = BindingFlags.Instance | BindingFlags.Public | BindingFlags.NonPublic
                | BindingFlags.Static;
            FieldInfo field = type.GetField(fieldName, bindFlags);
            return field.GetValue(instance);
        }
    }
}
