﻿using RimWorld;
using System.Collections.Generic;
using System.Linq;
using Verse;
using Verse.AI;

namespace PowerMill
{

    public class WorkGiver_LeadAnimalToMill : WorkGiver_Scanner
    {
        public override ThingRequest PotentialWorkThingRequest => ThingRequest.ForDef(PowerMillDefOf.PM_PowerMill);
        public override bool HasJobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            CompPowerMill comp = t.TryGetComp<CompPowerMill>();
            if (comp != null && comp.trunks.Any(x => x.worker is null) && comp.totalWorkers?.Count < 4)
            {
                CompAssignableToPawn_Animals compAnimals = t.TryGetComp<CompAssignableToPawn_Animals>();
                return compAnimals.AssignedPawns.Count(x => x.CurJobDef != PowerMillDefOf.PM_GeneratePower && x.mindState.lastJobTag == JobTag.Idle && pawn.CanReserve(x)) > 0;
            }
            return false;
        }
        public override Job JobOnThing(Pawn pawn, Thing t, bool forced = false)
        {
            CompAssignableToPawn_Animals comp = t.TryGetComp<CompAssignableToPawn_Animals>();
            var freeAnimals = comp.AssignedPawns.Where(x => x.CurJobDef != PowerMillDefOf.PM_GeneratePower
                && x.mindState.lastJobTag == JobTag.Idle);
            var compPowerMill = t.TryGetComp<CompPowerMill>();
            if (compPowerMill.totalWorkers is null)
            {
                compPowerMill.totalWorkers = new List<Pawn>();
            }
            var closestAnimal = GenClosest.ClosestThing_Global_Reachable(pawn.Position, pawn.Map, freeAnimals, PathEndMode.ClosestTouch, TraverseParms.For(pawn, Danger.Deadly), 9999f, (Thing x) => pawn.CanReserve(x)) as Pawn;
            if (closestAnimal != null)
            {
                return JobMaker.MakeJob(PowerMillDefOf.PM_RopeAnimalToPowerMill, closestAnimal, t);
            }
            return null;
        }
    }

}
